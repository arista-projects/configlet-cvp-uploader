Code documentation
==================

.. automodule:: cvpConfigletUploader
    :members: action_add, action_update, action_delete, action_create_change_control

Inventory Class
---------------

.. autoclass:: cvpConfigletUploader.CvpInventory
    :members:
    :show-inheritance:

Configlet Class
---------------

.. autoclass:: cvpConfigletUploader.CvpConfiglet
    :members:
    :show-inheritance:

Change Control Class
---------------------

.. autoclass:: cvpConfigletUploader.CvpChangeControl
    :members:
    :show-inheritance: