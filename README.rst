.. image:: https://img.shields.io/github/license/titom73/configlet-cvp-uploader .svg
  :target: https://github.com/titom73/configlet-cvp-uploader/blob/master/LICENSE
  :alt: License Model

.. image:: https://readthedocs.org/projects/arista-cvp-configlet-uploader/badge/?version=latest
  :target: https://arista-cvp-configlet-uploader.readthedocs.io/en/latest/?badge=latest
  :alt: Documentation Status

.. image:: https://travis-ci.org/titom73/configlet-cvp-uploader.svg?branch=master
    :target: https://travis-ci.org/titom73/configlet-cvp-uploader
    :alt: CI Status

Configlet uploader to CVP
=========================

Generic script to update configlet on an `Arista Cloudvision <https://www.arista.com/en/products/eos/eos-cloudvision>`_ server. It is based on `cvprac <https://github.com/aristanetworks/cvprac>`_ library to
interact using APIs calls between your client and CVP interface.

**Supported Features**

-  **Update** existing remote configlet.
-  Execute configlet update.
-  Wait for task result.
-  **Delete** configlet from server.
-  **Creating** a new Configlet.
- Add and remove devices to/from existing configlet.
-  Creating **change-control**.
-  **Scheduling** change-control.
-  Collect tasks to attach to change-control.

Complete documentation available on `read the doc <https://arista-cvp-configlet-uploader.readthedocs.io/en/latest/>`_

Known Issue
-----------

Due to a change in CVP API, change-control needs to get snapshot referenced per
task. Current version of ``cvprack`` does not support it in version 1.0.1

Fix is available in develop version. To install development version, use pip::

   $ pip install git+https://github.com/aristanetworks/cvprac.git@develop


Getting Started
---------------

.. code:: shell


   $ pip install git+https://github.com/titom73/configlet-cvp-uploader.git

   # Update your credential information
   $ cat <<EOT > env.variables.sh
   export CVP_HOST='13.57.194.119'
   export CVP_PORT=443
   export CVP_PROTO='https'
   export CVP_USER='username'
   export CVP_PASS='password'
   export CVP_TZ='France'
   export CVP_COUNTRY='France'
   EOT

   # run script (assuming VLANs configlet is present on CVP)
   $ cvp-configlet-uploader -c VLANs

License
-------

Project is published under `BSD License <https://github.com/titom73/configlet-cvp-uploader/blob/master/LICENSE>`_.

Ask question or report issue
----------------------------

Please open an issue on Github this is the fastest way to get an answer.

Contribute
----------

Contributing pull requests are gladly welcomed for this repository. If
you are planning a big change, please start a discussion first to make
sure we’ll be able to merge it.
