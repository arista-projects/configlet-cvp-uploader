Installation
---------------

Script can be used with 2 different installation method:

- git clone for testing. In this case it is recommended to use a virtual-environment
- Python PIP module to install binary directly to your syste. A virtual-environment is also recommended for testing purpose.

Installation with PIP
~~~~~~~~~~~~~~~~~~~~~

.. code:: shell


   $ pip install git+https://github.com/titom73/configlet-cvp-uploader.git

   # Update your credential information
   $ cat <<EOT > env.variables.sh
   export CVP_HOST='13.57.194.119'
   export CVP_PORT=443
   export CVP_PROTO='https'
   export CVP_USER='username'
   export CVP_PASS='password'
   EOT

   $ source env.variables.sh

   # Create Local configlet
   $ cat <<EOT > VLANs
   vlan 12
   !
   vlan 34
   !
   vlan 73
   !
   EOT

   # run script (assuming VLANs configlet is present on CVP)
   $ cvp-configlet-uploader -c VLANs

Git Clone
~~~~~~~~~

It is highly recommended to use Python virtual environment for testing

.. code:: shell

   $ git clone https://github.com/titom73/configlet-cvp-uploader.git

   $ pip install -r requirements.txt

   # Update your credential information
   $ cat <<EOT > env.variables.sh
   export CVP_HOST='13.57.194.119'
   export CVP_PORT=443
   export CVP_PROTO='https'
   export CVP_USER='username'
   export CVP_PASS='password'
   EOT

   $ source env.variables.sh

   # Create Local configlet
   $ cat <<EOT > VLANs
   vlan 12
   !
   vlan 34
   !
   vlan 73
   !
   EOT

   # run script (assuming VLANs configlet is present on CVP)
   $ python bin/cvpConfigletUploader.py -c VLANs

Known Issue
~~~~~~~~~~~

Due to a change in CVP API, change-control needs to get snapshot referenced per
task. Current version of ``cvprack`` does not support it in version 1.0.1

Fix is available in develop version. To install development version, use pip::

   $ pip install git+https://github.com/aristanetworks/cvprac.git@develop
